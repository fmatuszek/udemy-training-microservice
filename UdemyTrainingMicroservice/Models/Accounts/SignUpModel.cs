﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace UdemyTrainingMicroservice.Models.Accounts
{
	public class SignUpModel
	{
		[Required]
		[EmailAddress]
		[Display(Name = "Email")]
		public string Email { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[MinLength(8, ErrorMessage = "Password must be at least eight alphanumeric characters long!")]
		[MaxLength(8, ErrorMessage = "Password must be at least eight alphanumeric characters long!")]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessage = "Password and its confirmation do not match!")]
		public string ConfirmPassword { get; set; }
	}
}
