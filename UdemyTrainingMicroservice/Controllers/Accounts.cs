﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.AspNetCore.Identity.Cognito;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.AspNetCore.Identity;
using UdemyTrainingMicroservice.Models.Accounts;

namespace UdemyTrainingMicroservice.Controllers
{
	public class Accounts : Controller
	{
		private readonly SignInManager<CognitoUser> _signInManager;
		private readonly UserManager<CognitoUser> _userManager;
		private readonly CognitoUserPool _pool;

		public Accounts(SignInManager<CognitoUser> signInManager, UserManager<CognitoUser> userManager, CognitoUserPool pool)
		{
			_signInManager = signInManager;
			_userManager = userManager;
			_pool = pool;
		}

		public async Task<IActionResult> SignUp()
		{
			var model = new SignUpModel();
			return View(model);
		}

		[HttpPost]
		[ActionName("SignUp")]
		public async Task<IActionResult> SignUpPost(SignUpModel model)
		{
			if (ModelState.IsValid)
			{
				var user = _pool.GetUser(model.Email);
				if (user.Status != null)
				{
					ModelState.AddModelError("UserExists", "User with this email already exists!");
					return View(model);
				}

				user.Attributes.Add(CognitoAttribute.Name.AttributeName, model.Email);
				var createdUser = await _userManager.CreateAsync(user, model.Password);

				if (createdUser.Succeeded)
				{
					RedirectToAction("Confirm");
				}
			}
			return View(model);
		}

		[HttpGet]
		public async Task<IActionResult> Confirm(ConfirmModel model)
		{
			return View(model);
		}

		[HttpPost]
		[ActionName("Confirm")]
		public async Task<IActionResult> ConfirmPost(ConfirmModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
				if (user == null)
				{
					ModelState.AddModelError("NotFound", "A user with the given email was not found!");
					return View(model);
				}

				var result = await (_userManager as CognitoUserManager<CognitoUser>).ConfirmSignUpAsync(user, model.Code, true).ConfigureAwait(false);
				if (result.Succeeded)
				{
					return RedirectToAction("Index", "Home");
				}
				
				foreach (var error in result.Errors)
				{ 
					ModelState.AddModelError(error.Code, error.Description);
				}

				return View(model);
			}

			return View(model);
		}

		[HttpGet]
		public async Task<IActionResult> SignIn(SignInModel model)
		{
			return View(model);
		}

		[HttpPost]
		[ActionName("SignIn")]
		public async Task<IActionResult> SignInPost(SignInModel model)
		{
			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false).ConfigureAwait(false);
				if (result.Succeeded)
				{
					return RedirectToAction("Index", "Home");
				}
				else
				{
					ModelState.AddModelError("LoginError", "Email or password do not match!");
				}
			}

			return View("SignIn", model);
		}
	}
}
